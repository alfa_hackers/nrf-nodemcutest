#include <SPI.h>
#include <RF24.h>

const uint64_t address = 21071992;

typedef struct Message {
	uint8_t index;
//	uint8_t messageId;
//	uint64_t  timeStamp;
	uint8_t red;
	uint8_t green;
	uint8_t blue;

	void printMessage() {
		Serial.print("number: ");
		Serial.println(this->index);
//		Serial.print("timeStamp: ");
//		Serial.println(this->timeStamp);
//		Serial.print("MessageId: ");
//		Serial.println(this->timeStamp);
		Serial.println("\t\t ==Color==");
		Serial.print("\t\t Red: ");
		Serial.println(red);
		Serial.print("\t\t Green: ");
		Serial.println(green);
		Serial.print("\t\t Blue: ");
		Serial.println(blue);
	}
};

unsigned long previousMillis = 0;
const long interval = 50000;

RF24 radio(2, 15);
Message message;

uint8_t rgb[3] = { 255, 255, 255 };

void setup() {
	Serial.begin(115200);
	radio.begin();
//	message.messageId = 0;
	randomSeed(millis());
	radio.openWritingPipe(address);
}

void loop() {

	unsigned long currentMillis = millis();

	if ((currentMillis - previousMillis) >= interval) {
		previousMillis = currentMillis;
		message.index = 0;
//		message.timeStamp = currentMillis;
		message.red = (rand() % 255);
		message.green = (rand() % 255);
		message.blue = (rand() % 255);
		bool status = radio.write(&message, sizeof(message));
		if (status) {
			Serial.println("==Sent==");
		} else {
			Serial.println("==Failed==");
		}
		message.printMessage();
//		message.messageId += 1;
	}
}
